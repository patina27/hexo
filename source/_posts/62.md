---
title: "[OpenJudge 2405]Avoid The Lakes"
date: 2015-11-5 22:22:10
tags: ["OI","水题"]
---

## 描述
Farmer John's farm was flooded in the most recent storm, a fact only aggravated by the information that his cows are deathly afraid of water. His insurance agency will only repay him, however, an amount depending on the size of the largest ""lake"" on his farm.

The farm is represented as a rectangular grid with N (1 <= N <= 100) rows and M (1 <= M <= 100) columns. Each cell in the grid is either dry or submerged, and exactly K (1 <= K <= N*M) of the cells are submerged. As one would expect, a lake has a central cell to which other cells connect by sharing a long edge (not a corner). Any cell that shares a long edge with the central cell or shares a long edge with any connected cell becomes a connected cell and is part of the lake.

## 输入
* Line 1: Three space-separated integers: N, M, and K

* Lines 2..K+1: Line i+1 describes one submerged location with two space separated integers that are its row and column: R and C

## 输出
* Line 1: The number of cells that the largest lake contains.

## 样例输入
3 4 5
3 2
2 2
3 1
2 3
1 1

## 样例输出
4

## 提示
INPUT DETAILS:

The farm is a grid with three rows and four columns; five of the cells are submerged. They are located in the positions (row 3, column 2); (row 2, column 2); (row 3, column 1); (row 2, column 3); (row 1, column 1):
              # . . .
              . # # .
              # # . .

OUTPUT DETAILS:

The largest lake consists of the input's first four cells.
## 来源
USACO November 2007 Bronze


```
代码丢了。。。。
```