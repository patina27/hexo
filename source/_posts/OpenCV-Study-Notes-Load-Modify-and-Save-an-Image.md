---
title: 'OpenCV Study Notes: Load, Modify, and Save an Image'
date: 2016-10-19 20:23:30
tags: ["OpenCV","C++","VS"]
---
## cvtColor();
```cpp
cvtColor(sourse Mat, target Mat, instruction);
```
[instrunctions](http://docs.opencv.org/3.1.0/d7/d1b/group__imgproc__misc.html#gga4e0972be5de079fed4e3a10e24ef5ef0a353a4b8db9040165db4dacb5bcefb6ea)

COLOR_BGR2GRAY

## imwrite();
```cpp
imwrite(save location, source Mat);
```
