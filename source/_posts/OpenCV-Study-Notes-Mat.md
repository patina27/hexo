---
title: 'OpenCV Study Notes: Mat - The Basic Image Container'
date: 2016-10-19 3:05:29
tags: ["OpenCV","C++","VS"]
---
## Mat
**Mat is basically a class with two data parts:** the matrix header and a pointer to the actual matrix

**the copy operators will only copy the headers**, which means that modifying on object would affect other objects with the same data.
```cpp
Mat A, C;  // creates just the header parts
A = imread(argv[1], IMREAD_COLOR); // here we'll know the method used (allocate matrix)
Mat B(A);  // Use the copy constructor
C = A;  // Assignment operator
```
All the above objects, in the end, point to the same single data matrix.

**the headers are distinctive**, so we can create objects with different regions of interest (ROI) while using the same data.

```cpp
Mat D (A, Rect(10, 10, 100, 100) ); // using a rectangle
Mat E = A(Range::all(), Range(1,3)); // using row and column boundaries
```

To truly copy images is still posible by using the cv::Mat::clone() and cv::Mat::copyTo() methods.
```cpp
Mat F = A.clone();
Mat G;
A.copyTo(G);
```
Now modifying F or G will not affect the matrix pointed by the Mat header. 

## Storing Methods
**Several ways to store color:**  
1. RGB
2. HSV/HLS Hue, Saturation, and value/luminance
3. YCrCb 
4. CIE L\*a\*b\*