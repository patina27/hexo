---
title: 'OpenCV study notes: Load and Display an Image'
date: 2016-10-18 23:46:38
tags: ["OpenCV","C++","VS"]
---
## cv::imread()
	image = imread(imageName.c_str(), IMREAD_COLOR);
	cv::imread(path, type);
1. **IMREAD_UNCHANGED (\<0)** loads the image as is (including the alpha channel if present)  

2. **IMREAD_GRAYSCALE (0)** loads the image as an intensity one
3. **IMREAD_COLOR (\>0)** loads the image in the RGB format

## cv::namedWindow()
After checking that the image data was loaded correctly, we want to display our image, so we create an OpenCV window using the cv::namedWindow function. These are automatically managed by OpenCV once you create them. For this you need to specify its name and how it should handle the change of the image it contains from a size point of view. It may be:
1. WINDOW_AUTOSIZE
2. WINDOW_NORMAL  
```cpp
namedWindow( "Display window", WINDOW_AUTOSIZE ); // Create a window for display.
```  

## cv::imshow()
```cpp
  imshow( "Display window", image );                // Show our image inside it.
```

## cv::waitKey()
```cpp
waitKey(0); // Wait for a keystroke in the window
```
only parameter is just how long should it wait for a user input (measured in milliseconds)
